#!flask/bin/python

from flask.ext.script import Manager
from flask.ext.migrate import Migrate, MigrateCommand
import os

from app import myapp, db
myapp.config.from_object('config')

migrate = Migrate(myapp, db)
manager = Manager(myapp)

manager.add_command('db', MigrateCommand)

if __name__ == '__main__':
    manager.run()
