import os
from flask.ext.login import LoginManager
from flask.ext.openid import OpenID
#from config import basedir
from config import basedir, ADMINS, MAIL_PORT, MAIL_PASSWORD, MAIL_USERNAME, MAIL_SERVER

from flask import Flask
from flask.ext.sqlalchemy import SQLAlchemy

myapp = Flask(__name__)
myapp.config.from_object('config')
db = SQLAlchemy(myapp)
lm = LoginManager()
lm.init_app(myapp)
lm.login_view = 'login'
oid = OpenID(myapp, os.path.join(basedir, 'tmp'))


if not myapp.debug:
    import logging
    from logging.handlers import SMTPHandler
    credentials = None
    if MAIL_USERNAME or MAIL_PASSWORD:
        credentials = (MAIL_USERNAME, MAIL_PASSWORD)
    mail_handler = SMTPHandler((MAIL_SERVER, MAIL_PORT), 'no-reply@' + MAIL_SERVER, ADMINS, 'microblog failure', credentials)
    mail_handler.setLevel(logging.ERROR)
    myapp.logger.addHandler(mail_handler)


if not myapp.debug:
    import logging
    from logging.handlers import RotatingFileHandler
    file_handler = RotatingFileHandler('tmp/microblog.log', 'a', 1 * 1024 * 1024, 10)
    file_handler.setFormatter(logging.Formatter('%(asctime)s %(levelname)s: %(message)s [in %(pathname)s:%(lineno)d]'))
    myapp.logger.setLevel(logging.INFO)
    file_handler.setLevel(logging.INFO)
    myapp.logger.addHandler(file_handler)
    myapp.logger.info('microblog startup')


from app import views, models



